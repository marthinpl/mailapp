var app = angular.module("app", ['ui.router']);


app.config(function($stateProvider){
	$stateProvider
		.state('received', {
			url: '/received',
			templateUrl: 'partials/received.html'
		})

		.state('sent', {
			url: '/sent',
			templateUrl: 'partials/sent.html'
		})

		.state('received.id', {
			url: "/received/:emailId",
			templateUrl: 'single_mail.html'
			//cotnroller: function($stateParams)
		})

		.state('create', {
			url: "/create",
			templateUrl: '/partials/create.html'
		})
});

app.controller("PostsCtrl", function($scope, $http){
	//$http.defaults.headers.common["X-Custom-Header"] = "Angular.js";
	$http.get('posts.json').
		success(function(data, status, headers, config) {
			$scope.posts = data;
		});


});





app.controller('GetMails', function($scope, $http){
	/*$scope.getEmails = function () {*/
		$http.get('/emails').success(function(res) {
			$scope.emailsRes = res;
		});
/*	};*/
})


app.controller('GetSentMails', function($scope, $http){
	$http.get('/sent').success(function(res,status) {
		$scope.emailsRes = res;
	});
})


app.controller('CreateMail', function($scope, $http){
	$scope.createOne = function(){
        		$http.post('/sent', {
        			id: Date.now(),
        			title: $scope.sentTitle,
        			content: $scope.sentContent,
        			receivers: [$scope.receivers],
        			sent: Date.now()
        		}).success(function (res) {
        			$scope.createSentRes = res;
        		})

	};
});








